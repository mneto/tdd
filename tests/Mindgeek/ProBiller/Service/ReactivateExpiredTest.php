<?php

namespace Mindgeek\ProBiller\Service;
use Mindgeek\ProBiller\Entity\Subscription;

/**
 * Class ReactivateExpiredTest
 *
 * @package Mindgeek\ProBiller\Service
 * @coversDefaultClass \Mindgeek\ProBiller\Service\ReactivateExpired
 */
class ReactivateExpiredTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Mindgeek\ProBiller\Service\ReactivateExpired
     */
    protected $service;

    /**
     * Setting up the env
     *
     * @return void
     */
    protected function setUp()
    {
        $this->service = new ReactivateExpired();
    }

    /**
     * Test when the subscription is not expired
     *
     * @covers ::process
     * @return void
     */
   public function testProcessWithNonExpiredSubscriptionShouldReturnFalse()
   {
       $subscription = new Subscription();
       $subscription->setExpiryDate(new \DateTime('2065-12-31'));
       $rc = $this->service->process($subscription);
       $this->assertFalse($rc);
   }

    /**
     * Test when the subscription is expired
     *
     * @covers ::process
     * @return Subscription
     */
   public function testProcessWithExpiredSubscriptionShouldReturnTrue()
   {
       $subscription = new Subscription();
       $subscription->setExpiryDate(new \DateTime('1973-01-30'));
       $subscription->setSignedDays(30);
       $rc = $this->service->process($subscription);
       $this->assertTrue($rc);

       return $subscription;
   }

    /**
     * Test when the subscription is expired if it updates the expiraton date
     *
     * @param Subscription $subscription Subscription that was processed
     * @depends testProcessWithExpiredSubscriptionShouldReturnTrue
     * @covers ::process
     * @return void
     */
    public function testProcessWithExpiredSubscriptionShouldUpdateExpirationDate(Subscription $subscription)
    {
        $this->assertGreaterThan(new \DateTime(), $subscription->getExpiryDate());
    }
}