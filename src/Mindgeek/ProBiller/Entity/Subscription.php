<?php

namespace Mindgeek\ProBiller\Entity;

class Subscription
{
    const TRIAL = "trial";
    const FULL = "full";
    const EXPIRED = "expired";
    const ACTIVE = "active";

    /**
     *
     * @var integer
     */
    protected $id;
    
    /**
     *
     * @var string
     */
    protected $username;
    
    /**
     *
     * @var string
     */
    protected $password;
    
    /**
     *
     * @var string
     */
    protected $status;
    
    /**
     *
     * @var \DateTime
     */
    protected $expiryDate;
    
    /**
     *
     * @var integer
     */
    protected $signedDays;
    
    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * 
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * 
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getExpiryDate() {
        return $this->expiryDate;
    }

    /**
     * 
     * @return integer
     */
    public function getSignedDays() {
        return $this->signedDays;
    }

    /**
     * 
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $username
     */
    public function setUsername($username) {
        $this->username = $username;
    }

    /**
     * 
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }

    /**
     * 
     * @param string $status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * 
     * @param \DateTime $expiryDate
     */
    public function setExpiryDate(\DateTime $expiryDate) {
        $this->expiryDate = $expiryDate;
    }

    /**
     * 
     * @param integer $signedDays
     */
    public function setSignedDays($signedDays) {
        $this->signedDays = $signedDays;
    }
}
