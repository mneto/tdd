<?php

namespace Mindgeek\ProBiller\Service;
use Mindgeek\ProBiller\Entity\Subscription;

/**
 * Class UpgradeTrial
 *
 * @package Mindgeek\ProBiller\Service
 */
class UpgradeTrial
{
    /**
     * Converts the subscription from trial to full
     * 
     * @param Subscription $subscription Subscription to be upgraded
     * @return boolean
     */
    public function process(Subscription $subscription)
    {

    }
}
