<?php

namespace Mindgeek\ProBiller\Service;
use Mindgeek\ProBiller\Entity\Subscription;

/**
 * Class ReactivateExpired
 *
 * @package Mindgeek\ProBiller\Service
 */
class ReactivateExpired
{
    /**
     * Reactivates an expired subscription
     *
     * @param Subscription $subscription Subscription to be reactivated
     * @return boolean
     */
    public function process(Subscription $subscription)
    {
        $now = new \DateTime();
        if ($subscription->getExpiryDate() > $now) {
            return false;
        }

        $newExpiryDate = clone $now;
        $newExpiryDate->add(new \DateInterval("P" . $subscription->getSignedDays() ."D"));
        $subscription->setExpiryDate($newExpiryDate);
        return true;
    }
}